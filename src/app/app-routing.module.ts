import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FireworksComponent } from './components/fireworks/fireworks.component'
import { LoginComponent } from './components/login/login.component'
import { RegisterComponent} from './components/register/register.component'
import { PortfolioComponent } from './components/portfolio/portfolio.component'

const routes: Routes = [
 
    { path: 'portfolio', component: PortfolioComponent},
    { path: 'login', component: LoginComponent},
    { path: 'register', component:RegisterComponent, },
    { path: 'fireworks', component:FireworksComponent, },
  ];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
